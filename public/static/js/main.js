function nouveauCompte(type) {
    $('#nouveauCompte').modal('toggle');
    $('#compte_type').val(type);
}

function compenser(id, compteID) {
    var compenser = 0;

    if ($('#'+id).prop('checked')) {
        compenser = 1;
    }

    var ligneID = id.split('_');
    var requete = {
        "compteID": compteID,
        "ligneID": ligneID[1],
        "compenser": compenser
    }

    $.ajax({
        type : 'POST',
        timeout:60000,
        url : 'ajax/compenser.html',
        data : 'request='+JSON.stringify(requete),
        success : function (retour) {
            if (compenser == 1) {
                var val = $('#compense').html();
                compense = parseFloat(val.split(' €')[0])-parseFloat(retour.montant);

                var val = $('#non-compense').html();
                nonCompense = parseFloat(val.split(' €')[0])+parseFloat(retour.montant);

                $('#compense').html(compense+" €");
                $('#non-compense').html(nonCompense+" €");
            } else {
                var val = $('#compense').html();
                compense = parseFloat(val.split(' €')[0])+parseFloat(retour.montant);

                var val = $('#non-compense').html();
                nonCompense = parseFloat(val.split(' €')[0])-parseFloat(retour.montant);

                $('#compense').html(compense+" €");
                $('#non-compense').html(nonCompense+" €");
            }
        },
        error: function (retour) {
            if (compenser == 1) {
                $('#'+id).prop('checked', false)
            } else {
                $('#'+id).prop('checked', true)
            }
        }
    });
}

function modLigne(compteID, ligneID) {
    var requete = {
        "compteID": compteID,
        "ligneID": ligneID
    }

    $.ajax({
        type : 'POST',
        timeout:60000,
        url : 'ajax/getLigne.html',
        data : 'request='+JSON.stringify(requete),
        success : function (retour) {
            console.log(retour);

            $('#modType option[value="'+retour.type+'"]').prop('selected', true);
            $('#modTier').val(retour.tier.nom);
            $('#modDate').val(retour.date);
            $('#modCat').val(retour.categorie.nom);
            $('#modMontant').val(retour.montant);
            $('#modId').val(ligneID);

            $('#modLigne').modal('toggle');
        }
    });
}

function echPeriodicite(type) {
    var periodicite = $('#ech_periodicite_'+type).val();

    if (periodicite == "jours") {
        $('#ech_titre').html(' ');
        $('#ech_corps').html(' ');
    } else if (periodicite == "semaines") {
        $('#ech_titre').html('Choisissez le jour de la semaine');

        var html = '<select class="form-control" name="time">';
        html += '<option value="1">Lundi</option>';
        html += '<option value="2">Mardi</option>';
        html += '<option value="3">Mercredi</option>';
        html += '<option value="4">Jeudi</option>';
        html += '<option value="5">Vendredi</option>';
        html += '<option value="6">Samedi</option>';
        html += '<option value="7">Dimanche</option>';
        html += '</select>';

        $('#ech_corps').html(html);
    } else if (periodicite == "mois") {
        $('#ech_titre').html('Choisissez le jour du mois');

        var html = '<select class="form-control" name="time">';

        for (var i=1 ; i<= 30 ; i++) {
            html += '<option value="'+i+'">'+i+'</option>';
        }

        html += '<option value="31">Dernier jour du mois (30/31)</option>';
        html += '</select>';

        $('#ech_corps').html(html);
    } else if (periodicite == "ans") {
        $('#ech_titre').html('Choisissez le jour et le mois');

        var html = '<select class="form-control" style="width: 80px;display: inline;" name="time">';

        for (var i=1 ; i<= 30 ; i++) {
            html += '<option value="'+i+'">'+i+'</option>';
        }

        html += '<option value="31">Dernier jour du mois (30/31)</option>';
        html += '</select>';

        html += '<select class="form-control" style="width: 180px;display: inline;margin-left: 12px;" name="mois">';
        html += '<option value="01">Janvier</option>';
        html += '<option value="02">Février</option>';
        html += '<option value="03">Mars</option>';
        html += '<option value="04">Avril</option>';
        html += '<option value="05">Mai</option>';
        html += '<option value="06">Juin</option>';
        html += '<option value="07">Juillet</option>';
        html += '<option value="08">Août</option>';
        html += '<option value="09">Septembre</option>';
        html += '<option value="10">Octobre</option>';
        html += '<option value="11">Novembre</option>';
        html += '<option value="12">Décembre</option>';
        html += '</select>';

        $('#ech_corps').html(html);
    }
}

function modeEch(compte, id) {

    var requete = {
        "compteID": compte,
        "echID": id
    }

    $.ajax({
        type : 'POST',
        timeout:60000,
        url : 'ajax/getEcheance.html',
        data : 'request='+JSON.stringify(requete),
        success : function (retour) {
            console.log(retour);

            $('#modType option[value="'+retour.type+'"]').prop('selected', true);
            $('#ech_periodicite_mod option[value="'+retour.periodicite+'"]').prop('selected', true);
            $('#modTier').val(retour.tier.nom);
            $('#modDate_first').val(retour.dateDeb);
            $('#modCat').val(retour.categorie.nom);
            $('#modMontant').val(retour.montant);
            $('#modId').val(retour.id);

            if (retour.periodicite == "jours") {
                $('#ech_titre_mod').html(' ');
                $('#ech_corps_mod').html(' ');
            } else if (retour.periodicite == "semaines") {
                $('#ech_titre_mod').html('Choisissez le jour de la semaine');

                var html = '<select class="form-control" id="modTime" name="time">';
                html += '<option value="1">Lundi</option>';
                html += '<option value="2">Mardi</option>';
                html += '<option value="3">Mercredi</option>';
                html += '<option value="4">Jeudi</option>';
                html += '<option value="5">Vendredi</option>';
                html += '<option value="6">Samedi</option>';
                html += '<option value="7">Dimanche</option>';
                html += '</select>';

                $('#ech_corps_mod').html(html);

                $('#modTime option[value="'+retour.periode.jour+'"]').prop('selected', true);
            } else if (retour.periodicite == "mois") {
                $('#ech_titre_mod').html('Choisissez le jour du mois');

                var html = '<select class="form-control" id="modTime" name="time">';

                for (var i=1 ; i<= 30 ; i++) {
                    html += '<option value="'+i+'">'+i+'</option>';
                }

                html += '<option value="31">Dernier jour du mois (30/31)</option>';
                html += '</select>';

                $('#ech_corps_mod').html(html);

                $('#modTime option[value="'+retour.periode.jour+'"]').prop('selected', true);
            } else if (retour.periodicite == "ans") {
                $('#ech_titre_mod').html('Choisissez le jour et le mois');

                var html = '<select class="form-control" id="modTime" style="width: 80px;display: inline;" name="time">';

                for (var i=1 ; i<= 30 ; i++) {
                    html += '<option value="'+i+'">'+i+'</option>';
                }

                html += '<option value="31">Dernier jour du mois (30/31)</option>';
                html += '</select>';

                html += '<select class="form-control" id="modMois" style="width: 180px;display: inline;margin-left: 12px;" name="mois">';
                html += '<option value="01">Janvier</option>';
                html += '<option value="02">Février</option>';
                html += '<option value="03">Mars</option>';
                html += '<option value="04">Avril</option>';
                html += '<option value="05">Mai</option>';
                html += '<option value="06">Juin</option>';
                html += '<option value="07">Juillet</option>';
                html += '<option value="08">Août</option>';
                html += '<option value="09">Septembre</option>';
                html += '<option value="10">Octobre</option>';
                html += '<option value="11">Novembre</option>';
                html += '<option value="12">Décembre</option>';
                html += '</select>';

                $('#ech_corps_mod').html(html);

                $('#modTime option[value="'+retour.periode.jour+'"]').prop('selected', true);
                $('#modMois option[value="'+retour.periode.mois+'"]').prop('selected', true);
            }

            $('#modEcheance').modal('toggle');
        }
    });
}

function budgetDelCat(compteID, budgetID, catID) {
    var requete = {
        "compteID": compteID,
        "budgetID": budgetID,
        "catID": catID
    }

    $.ajax({
        type : 'POST',
        timeout:60000,
        url : 'ajax/budget/delCat.html',
        data : 'request='+JSON.stringify(requete),
        success : function (retour) {
            $('#cat_'+catID).remove();

            if (retour == "delete") {
                location.replace('comptes/'+compteID+'/rapport-all.html');
            }
        }
    });

    return false;
}

function disableAutocomplete(elementId)
{
    var e = document.getElementById(elementId);
    if(e != null)
    {
        e.setAttribute("autocomplete", "off");
    }
}

function loadLines(compteID, tri) {
    var requete = {
        "compteID": compteID,
        "tri": tri
    }

    $.ajax({
        type : 'POST',
        timeout:60000,
        url : 'ajax/load-lines.html',
        data : 'request='+JSON.stringify(requete),
        success : function (retour) {
            $('#lines-loader').remove();

            for (var each in retour) {
                var html = '<tr><td class="text-center">' +
                    '<div class="dropdown"><a href="#" type="button" id="afficher" data-toggle="dropdown" class="ligne-edit"><span class="glyphicon glyphicon-edit"></span><br></a>' +
                    '<ul class="dropdown-menu" role="menu" aria-labelledby="afficher"><li role="presentation"><a onclick="modLigne(\''+compteID+'\', \''+retour[each].id+'\');">Modifier</a></li> ' +
                    '<li role="presentation"><a href="comptes/'+compteID+'/del-'+retour[each].id+'-'+tri+'.html">Supprimer</a></li>' +
                '</ul></div></td> ' +
                '<td>'+retour[each].date+'</td> ' +
                '<td>'+retour[each].categorie.nom+'</td>' +
                '<td>'+retour[each].tier.nom+'</td>';

                if (retour[each].type == "debit") {
                    html += '<td>'+retour[each].montant+' €</td><td></td>';
                } else {
                    html += '<td></td><td>'+retour[each].montant+' €</td>';
                }

                if (retour[each].compense == "1") {
                    html += '<td><input type="checkbox" id="ligne_'+retour[each].id+'" onclick="compenser(\'ligne_'+retour[each].id+'\', \''+compteID+'\');" checked></td></tr>';
                } else {
                    html += '<td><input type="checkbox" id="ligne_'+retour[each].id+'" onclick="compenser(\'ligne_'+retour[each].id+'\', \''+compteID+'\');"></td></tr>';
                }

                $('#lines-table').prepend(html);
            }
        }
    });
}