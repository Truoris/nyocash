<?php
class Qif extends BaseController {

    public function import($compteID) {
        if (Input::hasFile('fichier')) {
            $fichier = Input::file('fichier');

            $mime = $fichier->getMimeType();
            $extension = $fichier->getClientOriginalExtension();

            if (strtoupper($extension) == "QIF" AND $mime == "text/plain") {
                $compte = Compte::where('owner', '=', Auth::user()->id)->where('id', '=', $compteID)->first();

                $filename = 'user-'.Auth::user()->id.'-'.time().'.QIF';
                $fichier->move('file/import', $filename);

                $file = File::get('file/import/'.$filename);

                $fichier = explode('!Type:Bank', $file);

                $lignes = explode('^', $fichier[1]);

                array_pop($lignes);

                $i = 0;
                foreach ($lignes as $ligne) {
                    $i++;

                    $ligne = str_replace(CHR(10),"|||",$ligne);
                    $ligne = str_replace(CHR(13),"|||",$ligne);
                    $info = explode('|||', $ligne);

                    $data['compense'] = 0;
                    $data['date'] = 123456789;
                    $data['montant'] = 42.0;
                    $data['tier'] = "default";
                    $data['categorie'] = "default";

                    foreach ($info as $each) {
                        $type = str_split($each);

                        if ($type[0] != "") {
                            if ($type[0] == "D") {
                                $date = explode('.', substr($each,1));
                                $data['date'] = mktime('12', '0', '0', $date[1], $date[0], $date[2]);
                            } elseif ($type[0] == "T") {
                                $data['montant'] = str_replace(',', '.', substr($each,1));
                            } elseif ($type[0] == "C") {
                                $compense = substr($each,1);

                                $data['compense'] = 1;

                                if ($compense == "X") {
                                    $data['compense'] = 2;
                                }
                            } elseif ($type[0] == "P") {
                                $data['tier'] = substr($each,1);
                            } elseif ($type[0] == "L") {
                                $data['categorie'] = substr($each,1);
                            }
                        }
                    }

                    if ($data['compense'] == 2) {
                        $data['tier'] = "Moi";
                        $data['categorie'] = "Ouverture";
                        $data['compense'] = 1;
                    }

                    if($data['date'] == 123456789) {
                        echo $ligne;
                    }

                    $ligne = new Ligne();

                    $ligne->compte = $compte['id'];

                    $tier = Tier::where('nom', '=', $data['tier'])->where('owner', '=', Auth::user()->id)->first();

                    if (count($tier) == 0) {
                        $tier = new Tier();

                        $tier->owner = Auth::user()->id;
                        $tier->nom = $data['tier'];
                        $tier->timestamp = time();

                        $tier->save();
                    }

                    $ligne->tier = $tier->id;

                    $ligne->type = "debit";
                    if ($data['montant'] >= 0) {
                        $ligne->type = "credit";
                    }

                    $cat = Categorie::where('nom', '=', $data['categorie'])->where('owner', '=', Auth::user()->id)->first();

                    if (count($cat) == 0) {
                        $cat = new Categorie();

                        $cat->owner = Auth::user()->id;
                        $cat->nom = $data['categorie'];
                        $cat->timestamp = time();

                        $cat->save();
                    }

                    $ligne->categorie = $cat->id;
                    $ligne->date = $data['date'];

                    $ligne->compense = $data['compense'];
                    $ligne->montant = $data['montant'];

                    $ligne->save();

                    unset($data);
                }

                $solde = round(Ligne::where('compte', '=', $compte['id'])->sum('montant'), 2);

                Compte::where('owner', '=', Auth::user()->id)->where('id', '=', $compte['id'])->update(array('solde' => $solde));

                unlink('file/import/'.$filename);

                return Redirect::to('/comptes/'.$compteID.'/view-all.html');
            } else {
                return Redirect::to('/comptes/'.$compteID.'/view-all.html');
            }
        } else {
            return Redirect::to('/comptes/'.$compteID.'/view-all.html');
        }

    }

    public function export($compteID) {
        if (Input::has('from')) {
            $from = explode('/', Input::get('from'));
            $debut = mktime(0, 0, 0, $from[1], $from[0], $from[2]);

            $to = explode('/', Input::get('to'));
            $fin = mktime(23, 59, 59, $to[1], $to[0], $to[2]);

            $compte = Compte::where('owner', '=', Auth::user()->id)->where('id', '=', $compteID)->first();

            $lignes = Ligne::where('compte', '=', $compte['id'])->where('date', '>=', $debut)->where('date', '<=', $fin)->with('Tier', 'Categorie')->get();

            $fileName = 'file/export/user-'.Auth::user()->id.'-'.time().'.QIF';

            $i = 0;

            $qif = "";

            foreach ($lignes as $ligne) {
                if ($i == 0) {
                    $dateFirst = $ligne->date;
                    $i++;
                }

                $qif .= "D".date('d.m.Y', $dateFirst)."\n";

                if ($ligne->compense == 1) {
                    $qif .= "C*\n";
                }

                $qif .= "T".$ligne->montant."\n";
                $qif .= "N".$ligne->id."\n";
                $qif .= "P".$ligne->Tier->nom."\n";
                $qif .= "L".$ligne->Categorie->nom."\n";
                $qif .= "^\n";
            }

            $content = "!Type:Bank\n";

            $content .= "D".date('d.m.Y', $dateFirst)."\n";
            $content .= "T0.00\n";
            $content .= "CX\n";
            $content .= "POpening Balance\n";
            $content .= "L[".$compte['nom']."]\n^\n";
            $content .= $qif;

            File::put($fileName, $content);

            return Response::download($fileName, 'export-'.$compte['nom'].'.QIF');
        } else {
            return Redirect::to('/comptes/'.$compteID.'/view-all.html');
        }
    }
}