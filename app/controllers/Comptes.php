<?php
class Comptes extends BaseController {

    public function liste() {
        $user['pseudo'] = Auth::user()->login;

        if (Input::has('nom')) {
            $compte = new Compte();

            $compte->owner = Auth::user()->id;
            $compte->nom = Input::get('nom');
            $compte->type = Input::get('type');
            $compte->solde = 0;

            $compte->save();
        }

        $comptes = Compte::where('owner', '=', Auth::user()->id)->get();

        $compteListe['courant'] = array();
        $compteListe['epargne'] = array();
        $compteListe['pro'] = array();
        $compteListe['total']['courant'] = 0;
        $compteListe['total']['epargne'] = 0;
        $compteListe['total']['pro'] = 0;


        foreach ($comptes as $compte) {
            $compte->solde = round($compte->solde, 2);
            array_push($compteListe[$compte->type], $compte);

            $compteListe['total'][$compte->type] += $compte->solde;
        }

        $compteListe['total']['total'] = $compteListe['total']['courant']+$compteListe['total']['epargne']+$compteListe['total']['pro'];

        return View::make('comptes_liste', array('url' => Config::get('app.url'), 'user' => $user, 'compteListe' => $compteListe));
    }

    public function view($id, $tri) {
        $user['pseudo'] = Auth::user()->login;

        $compte = Compte::where('id', '=', $id)->where('owner', '=', Auth::user()->id)->first();

        if (Input::has('nouvelle') AND Input::has('montant') AND Input::get('montant') > 0) {
            $type = Input::get('type');
            $montant = Input::get('montant');

            if ($type == "debit") {
                $montant *= -1;
            }

            $ligne = new Ligne();

            $ligne->compte = $compte['id'];

            $tier = Tier::where('nom', '=', Input::get('tier'))->where('owner', '=', Auth::user()->id)->first();

            if (count($tier) == 0) {
                $tier = new Tier();

                $tier->owner = Auth::user()->id;
                $tier->nom = Input::get('tier');
                $tier->timestamp = time();

                $tier->save();
            }

            $ligne->tier = $tier->id;

            $cat = Categorie::where('nom', '=', Input::get('cat'))->where('owner', '=', Auth::user()->id)->first();

            if (count($cat) == 0) {
                $cat = new Categorie();

                $cat->owner = Auth::user()->id;
                $cat->nom = Input::get('cat');
                $cat->timestamp = time();

                $cat->save();
            }

            $ligne->categorie = $cat->id;

            $ligne->type = $type;

            $date = explode('/', Input::get('date'));
            $ligne->date = mktime(12, 0, 0, $date[1], $date[0], $date[2]);

            $ligne->compense = 0;
            $ligne->montant = $montant;

            $ligne->save();

            $compte->solde += $montant;
        }

        if (Input::has('modifier')) {
            $type = Input::get('type');
            $montant = Input::get('montant');

            if ($type == "debit") {
                $montant *= -1;
            }

            $tier = Tier::where('nom', '=', Input::get('tier'))->where('owner', '=', Auth::user()->id)->first();

            if (count($tier) == 0) {
                $tier = new Tier();

                $tier->owner = Auth::user()->id;
                $tier->nom = Input::get('tier');
                $tier->timestamp = time();

                $tier->save();
            }

            $cat = Categorie::where('nom', '=', Input::get('cat'))->where('owner', '=', Auth::user()->id)->first();

            if (count($cat) == 0) {
                $cat = new Categorie();

                $cat->owner = Auth::user()->id;
                $cat->nom = Input::get('cat');
                $cat->timestamp = time();

                $cat->save();
            }

            $date = explode('/', Input::get('date'));
            $date = mktime(12, 0, 0, $date[1], $date[0], $date[2]);

            Ligne::where('compte', '=', $compte['id'])->where('id', '=', Input::get('id'))->update(array(
                'type' => $type,
                'tier' => $tier->id,
                'categorie' => $cat->id,
                'date' => $date,
                'montant' => $montant
            ));
        }

        $limite = mktime(0, 0, 0, date('m')-1, 1, date('Y'));

        $lignes = Ligne::where('compte', '=', $compte['id'])->where('date', '>', $limite)->orderBy('date')->with('Tier', 'Categorie');
        $solde = Ligne::where('compte', '=', $compte['id']);

        if ($tri == "compenser") {
            $lignes =  $lignes->where('compense', '=', 1);
            $solde = $solde->where('compense', '=', 1);
        } elseif ($tri == "non-compenser") {
            $lignes =  $lignes->where('compense', '=', 0);
            $solde = $solde->where('compense', '=', 0);
        }

        $lignes = $lignes->get();

        $total['lignes'] = 0;

        if (count($lignes) < Ligne::where('compte', '=', $compte['id'])->count()) {
            $total['lignes'] = 1;
        }

        foreach ($lignes as $ligne) {
            $ligne->montant = abs($ligne->montant);
        }

        $tiersListe = Tier::where('owner', '=', Auth::user()->id)->get();
        $catListe = Categorie::where('owner', '=', Auth::user()->id)->get();



        $compte['solde'] = round($solde->sum('montant'), 2);

        $total['compense'] = round(Ligne::where('compte', '=', $compte['id'])->where('compense', '=', 1)->sum('montant'), 2);
        $total['nonCompense'] = round(Ligne::where('compte', '=', $compte['id'])->where('compense', '=', 0)->sum('montant'), 2);
        $total['compte'] = round($total['compense']+$total['nonCompense'], 2);

        Compte::where('owner', '=', Auth::user()->id)->where('id', '=', $compte['id'])->update(array('solde' => $total['compte']));

        return View::make('view_compte', array(
                    'url'           => Config::get('app.url'),
                    'user'          => $user,
                    'compte'        => $compte,
                    'lignes'        => $lignes,
                    'tiersListe'    => $tiersListe,
                    'catListe'      => $catListe,
                    'tri'           => $tri,
                    'total'         => $total
        ));
    }

    public function categories() {
        $user['pseudo'] = Auth::user()->login;

        if (Input::has('add')) {
            $cat = new Categorie();

            $cat->owner = Auth::user()->id;
            $cat->nom = Input::get('nom');

            $cat->save();
        }

        if (Input::has('mod')) {
            Categorie::where('id', '=', Input::get('id'))->update(array('nom' => Input::get('nom')));
        }

        $cat = DB::select(DB::raw("SELECT *, (SELECT COUNT(*) FROM lignes WHERE categories.id = lignes.categorie) AS utilisation FROM categories WHERE owner = ".Auth::user()->id));

        return View::make('categories', array('url' => Config::get('app.url'), 'user' => $user, 'catListe' => $cat));
    }

    public function delete($compteID, $ligneID, $tri) {
        $compte = Compte::where('owner', '=', Auth::user()->id)->where('id', '=', $compteID)->first();

        Ligne::where('compte', '=', $compte['id'])->where('id', '=', $ligneID)->delete();

        return Redirect::to('/comptes/'.$compte['id'].'/view-'.$tri.'.html');
    }

    public function echeancier($compteID) {
        $user['pseudo'] = Auth::user()->login;

        $compte = Compte::where('id', '=', $compteID)->where('owner', '=', Auth::user()->id)->first();

        if (Input::has('nouveau')) {

            $tier = Tier::where('nom', '=', Input::get('tier'))->where('owner', '=', Auth::user()->id)->first();

            if (count($tier) == 0) {
                $tier = new Tier();

                $tier->owner = Auth::user()->id;
                $tier->nom = Input::get('tier');
                $tier->timestamp = time();

                $tier->save();
            }

            $cat = Categorie::where('nom', '=', Input::get('cat'))->where('owner', '=', Auth::user()->id)->first();

            if (count($cat) == 0) {
                $cat = new Categorie();

                $cat->owner = Auth::user()->id;
                $cat->nom = Input::get('cat');
                $cat->timestamp = time();

                $cat->save();
            }

            $date = explode('/', Input::get('date_first'));
            $date = mktime(12, 0, 0, $date[1], $date[0], $date[2]);

            $montant = Input::get('montant');

            if (Input::get('type') == "debit") {
                $montant *= -1;
            }

            if (Input::get('periodicite') == "jours") {
                $periode = "";
            } elseif (Input::get('periodicite') == "semaines") {
                $periode['jour'] = intval(Input::get('time'));
            } elseif (Input::get('periodicite') == "mois") {
                $periode['jour'] = intval(Input::get('time'));
            } elseif (Input::get('periodicite') == "ans") {
                $periode['jour'] = intval(Input::get('time'));
                $periode['mois'] = intval(Input::get('mois'));
            }

            $ech = new Echeance();

            $ech->compte = $compte['id'];
            $ech->tier = $tier->id;
            $ech->categorie = $cat->id;
            $ech->type = Input::get('type');
            $ech->montant = $montant;
            $ech->date_debut = $date;
            $ech->periodicite = Input::get('periodicite');
            $ech->periode = json_encode($periode, JSON_UNESCAPED_UNICODE);
            $ech->last_entry = 0;

            $ech->save();
        }

        if (Input::has('modifier')) {
            $tier = Tier::where('nom', '=', Input::get('tier'))->where('owner', '=', Auth::user()->id)->first();

            if (count($tier) == 0) {
                $tier = new Tier();

                $tier->owner = Auth::user()->id;
                $tier->nom = Input::get('tier');
                $tier->timestamp = time();

                $tier->save();
            }

            $cat = Categorie::where('nom', '=', Input::get('cat'))->where('owner', '=', Auth::user()->id)->first();

            if (count($cat) == 0) {
                $cat = new Categorie();

                $cat->owner = Auth::user()->id;
                $cat->nom = Input::get('cat');
                $cat->timestamp = time();

                $cat->save();
            }

            $date = explode('/', Input::get('date_first'));
            $date = mktime(12, 0, 0, $date[1], $date[0], $date[2]);

            $montant = Input::get('montant');

            if (Input::get('type') == "debit") {
                $montant *= -1;
            }

            if (Input::get('periodicite') == "jours") {
                $periode = "";
            } elseif (Input::get('periodicite') == "semaines") {
                $periode['jour'] = intval(Input::get('time'));
            } elseif (Input::get('periodicite') == "mois") {
                $periode['jour'] = intval(Input::get('time'));
            } elseif (Input::get('periodicite') == "ans") {
                $periode['jour'] = intval(Input::get('time'));
                $periode['mois'] = intval(Input::get('mois'));
            }

            $ech = new Echeance();

            $ech->compte = $compte['id'];
            $ech->tier = $tier->id;
            $ech->categorie = $cat->id;
            $ech->type = Input::get('type');
            $ech->montant = $montant;
            $ech->date_debut = $date;
            $ech->periodicite = Input::get('periodicite');
            $ech->periode = json_encode($periode, JSON_UNESCAPED_UNICODE);

            Echeance::where('compte', '=', $compte['id'])->where('id', '=', Input::get('id'))->update(array(
                'tier' => $tier->id,
                'categorie' => $cat->id,
                'type' => Input::get('type'),
                'montant' => $montant,
                'date_debut' => $date,
                'periodicite' => Input::get('periodicite'),
                'periode' => json_encode($periode, JSON_UNESCAPED_UNICODE)
            ));
        }

        $ech = Echeance::where('compte', '=', $compte['id'])->with('Tier', 'Categorie')->get();

        $jour = ['', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi', 'dimanche'];
        foreach ($ech as $each) {
            $each->montant = abs($each->montant);
            $periode = json_decode($each->periode);

            if ($each->periodicite == "jours") {
                $each->periodicite = "Tous les jours";
            } elseif ($each->periodicite == "semaines") {
                $each->periodicite = "Toutes les semaine le ".$jour[$periode->jour];
            } elseif ($each->periodicite == "mois") {
                $each->periodicite = "Tous les mois le ".$periode->jour;

                if ($periode->jour == 31) {
                    $each->periodicite = "Tous les mois le dernier jour";
                }
            } elseif ($each->periodicite == "ans") {
                $each->periodicite = "Tous les ans le ".$periode->jour."/".$periode->mois;
            }
        }

        $tiersListe = Tier::where('owner', '=', Auth::user()->id)->get();
        $catListe = Categorie::where('owner', '=', Auth::user()->id)->get();

        return View::make('echeancier', array(
            'url' => Config::get('app.url'),
            'user' => $user,
            'compte' => $compte,
            'ech' => $ech,
            'tiersListe' => $tiersListe,
            'catListe' => $catListe
        ));
    }

    public function delCat($id) {
        Categorie::where('owner', '=', Auth::user()->id)->where('id', '=', $id)->delete();

        return Redirect::to('/categories');
    }

    public function delEch($compteID, $id) {
        $compte = Compte::where('id', '=', $compteID)->where('owner', '=', Auth::user()->id)->first();

        Echeance::where('compte', '=', $compte['id'])->where('id', '=', $id)->delete();

        return Redirect::to('/comptes/'.$compte['id'].'/echeancier.html');
    }
}