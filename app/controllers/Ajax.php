<?php
class Ajax extends BaseController {

    public function compenser() {
        if (Auth::check()) {
            if (Input::has('request')) {
                $requete = json_decode(Input::get('request'));

                $compte = Compte::where('id', '=', $requete->compteID)->where('owner', '=', Auth::user()->id)->first();

                if (count($compte) == 1) {
                    Ligne::where('compte', '=', $compte['id'])->where('id', '=', $requete->ligneID)->update(array('compense' => $requete->compenser));
                    $ligne = Ligne::where('compte', '=', $compte['id'])->where('id', '=', $requete->ligneID)->first();

                    return Response::json(array("montant" => $ligne->montant));
                } else {
                    App::abort(401);
                }
            } else {
                App::abort(400, 'request input is missing');
            }
        } else {
            App::abort(403, 'You have to be authenticate.');
        }
    }

    public function getLigne() {
        if (Auth::check()) {
            if (Input::has('request')) {
                $requete = json_decode(Input::get('request'));

                $compte = Compte::where('id', '=', $requete->compteID)->where('owner', '=', Auth::user()->id)->first();

                $ligne = Ligne::where('compte', '=', $compte['id'])->where('id', '=', $requete->ligneID)->with('Tier', 'Categorie')->first();

                $ligne['date'] = $ligne->DateFormat;
                $ligne['montant'] = abs($ligne['montant']);

                return Response::json($ligne);
            } else {
                App::abort(400, 'request input is missing');
            }
        } else {
            App::abort(403, 'You have to be authenticate.');
        }
    }

    public function getEcheance() {
        if (Auth::check()) {
            if (Input::has('request')) {
                $requete = json_decode(Input::get('request'));

                $compte = Compte::where('id', '=', $requete->compteID)->where('owner', '=', Auth::user()->id)->first();

                $ech = Echeance::where('compte', '=', $compte['id'])->where('id', '=', $requete->echID)->with('Tier', 'Categorie')->first();

                $ech['periode'] = json_decode($ech['periode']);
                $ech['montant'] = abs($ech['montant']);
                $ech['dateDeb'] = $ech['dateDeb'];

                return Response::json($ech);
            } else {
                App::abort(400, 'request input is missing');
            }
        } else {
            App::abort(403, 'You have to be authenticate.');
        }
    }

    public function budgetDelCat() {
        if (Auth::check()) {
            if (Input::has('request')) {
                $requete = json_decode(Input::get('request'));

                $compte = Compte::where('id', '=', $requete->compteID)->where('owner', '=', Auth::user()->id)->first();
                $budget = Budget::where('compte', '=', $compte['id'])->where('id', '=', $requete->budgetID)->first();

                BudgetCategorie::where('budget_id', '=', $budget['id'])->where('categorie_id', '=', $requete->catID)->delete();

                $nbre = BudgetCategorie::where('budget_id', '=', $budget['id'])->count();

                if ($nbre == 0) {
                    Budget::where('compte', '=', $compte['id'])->where('id', '=', $requete->budgetID)->delete();
                    return "delete";
                }

                return ;
            } else {
                App::abort(400, 'request input is missing');
            }
        } else {
            App::abort(403, 'You have to be authenticate.');
        }
    }

    public function loadLines() {
        if (Auth::check()) {
            if (Input::has('request')) {
                $requete = json_decode(Input::get('request'));

                $compte = Compte::where('id', '=', $requete->compteID)->where('owner', '=', Auth::user()->id)->first();

                $limite = mktime(0, 0, 0, date('m')-1, 1, date('Y'));

                $json = Ligne::where('compte', '=', $compte['id'])
                    ->where('date', '<=', $limite)
                    ->orderBy('date', 'DESC')
                    ->with('Tier', 'Categorie');

                if ($requete->tri == 'compenser') {
                    $json = $json->where('compense', '=', 1);
                } else if ($requete->tri == 'non-compenser') {
                    $json = $json->where('compense', '=', 0);
                }

                $json = $json->get();

                foreach ($json as $ligne) {
                    $ligne->date = $ligne->dateFormat;
                    $ligne->montant = abs($ligne->montant);
                }

                return Response::json($json);
            } else {
                App::abort(400, 'request input is missing');
            }
        } else {
            App::abort(403, 'You have to be authenticate.');
        }
    }
}