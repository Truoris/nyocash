<?php
class Rapport extends BaseController {

    public function index($compteID, $budgetID) {
        $compte = Compte::where('owner', '=', Auth::user()->id)->where('id', '=', $compteID)->first();
        $user['pseudo'] = Auth::user()->login;

        if (Input::has('libele')) {
            $budget = new Budget();

            $budget->compte = $compte['id'];
            $budget->nom = Input::get('libele');

            $budget->save();

            foreach (Input::get('cats') as $cat) {
                $link = new BudgetCategorie();

                $link->budget_id = $budget->id;
                $link->categorie_id = $cat;

                $link->save();
            }
        }

        if (Input::has('addCat')) {
            foreach (Input::get('cats') as $cat) {
                $link = new BudgetCategorie();

                $link->budget_id = $budgetID;
                $link->categorie_id = $cat;

                $link->save();
            }
        }

        $budget_liste = Budget::where('compte', '=', $compte['id'])->with('categories')->get();

        $catListe = array();
        array_push($catListe, 0);
        foreach ($budget_liste as $budget) {
            foreach ($budget->categories as $cat) {
                array_push($catListe, $cat->id);
            }
        }

        $categories = Categorie::where('owner', '=', Auth::user()->id)->whereNotIn('id', $catListe)->get();

        if (Input::has('debut')) {
            $debut = explode('/', Input::get('debut'));
            Session::put('periodeDebut', mktime(0, 0, 0, $debut[1], $debut[0], $debut[2]));

            $fin = explode('/', Input::get('fin'));
            Session::put('periodeFin', mktime(0, 0, 0, $fin[1], $fin[0], $fin[2]));

            $periode['debut'] = Input::get('debut');
            $periode['fin'] = Input::get('fin');
        } else {
            if (Session::has('periodeDebut')) {
                $periode['debut'] = date('d/m/Y', Session::get('periodeDebut'));
                $periode['fin'] = date('d/m/Y', Session::get('periodeFin'));
            } else {
                $anDebut = date('Y')-1;
                $periode['debut'] = date('d/m')."/".$anDebut;
                $periode['fin'] = date('d/m/Y');

                Session::put('periodeDebut', mktime(0, 0, 0, date('m'), date('d'), $anDebut));
                Session::put('periodeFin', mktime(0, 0, 0, date('m'), date('d'), date('Y')));
            }
        }

        if ($budgetID == "all") {
            ############## Categorie ##############
            $lignes = Ligne::where('compte', '=', $compte['id'])
                ->where('type', '=', 'debit')
                ->where('date', '>=', Session::get('periodeDebut'))
                ->where('date', '<=', Session::get('periodeFin'))
                ->with('Categorie')
                ->get();

            $info['byCat'] = array();

            foreach($lignes as $ligne) {
                if (isset($info['byCat'][$ligne->Categorie->id])) {
                    $info['byCat'][$ligne->Categorie->id]['total'] += abs($ligne->montant);
                } else {
                    $info['byCat'][$ligne->Categorie->id]['total'] = abs($ligne->montant);
                }

                $info['byCat'][$ligne->Categorie->id]['nom'] = $ligne->Categorie->nom;
            }
            #######################################
            ################ Budget ###############

            $info['byBudget'] = array();
            $somme = 0;

            foreach ($budget_liste as $budget) {
                $catListe = array();
                foreach ($budget->categories as $cat) {
                    array_push($catListe, $cat->id);
                }

                $temp['nom'] = $budget->nom;
                $montant = Ligne::where('compte', '=', $compte['id'])
                    ->where('type', '=', 'debit')
                    ->where('date', '>=', Session::get('periodeDebut'))
                    ->where('date', '<=', Session::get('periodeFin'))
                    ->whereIn('categorie', $catListe)
                    ->sum('montant');
                $temp['montant'] = abs($montant);
                $somme += $temp['montant'];

                $string = md5(time().rand(0, 999));
                $temp['color'] = "#".substr($string,0,2).substr($string,2,2).substr($string,4,2);

                array_push($info['byBudget'], $temp);
            }

            $temp['nom'] = "Autres";
            $temp['montant'] = abs(Ligne::where('compte', '=', $compte['id'])
                    ->where('type', '=', 'debit')
                    ->where('date', '>=', Session::get('periodeDebut'))
                    ->where('date', '<=', Session::get('periodeFin'))
                    ->sum('montant'))-$somme;

            $string = md5(time().rand(0, 999));
            $temp['color'] = "#".substr($string,0,2).substr($string,2,2).substr($string,4,2);

            array_push($info['byBudget'], $temp);

            #######################################
            ################# Tous ################

            $lignes = Ligne::where('compte', '=', $compte['id'])
                ->where('type', '=', 'debit')
                ->where('tier', '!=', 90)     // temporary fix
                ->where('date', '>=', Session::get('periodeDebut'))
                ->where('date', '<=', Session::get('periodeFin'))
                ->get();

            $mois = array();

            foreach ($lignes as $ligne) {
                if (isset($mois[date('m/Y', $ligne->date)])) {
                    $mois[date('m/Y', $ligne->date)] += abs($ligne->montant);
                } else {
                    $mois[date('m/Y', $ligne->date)] = abs($ligne->montant);
                }
            }

            $moy['somme'] = 0;
            $moy['nbre'] = 0;

            foreach($mois as $each) {
                $moy['somme'] += $each;
                $moy['nbre']++;
            }

            $info['all']['moyenne'] = round($moy['somme']/$moy['nbre'], 2);


            $info['all']['label'] = '';
            $info['all']['valeur'] = '';


            $info['all']['nbreValeur'] = count($mois);

            $debut['mois'] = date('m', Session::get('periodeDebut'));
            $debut['ans'] = date('Y', Session::get('periodeDebut'));

            $fin['mois'] = date('m', Session::get('periodeFin'));
            $fin['ans'] = date('Y', Session::get('periodeFin'));

            $current = $debut;
            $info['all']['nbreValeur'] = 0;

            do {
                $info['all']['label'] .= '"'.sprintf("%02d", $current['mois']).'/'.$current['ans'].'",';
                if (isset($mois[sprintf("%02d", $current['mois']).'/'.$current['ans']])) {
                    $info['all']['valeur'] .= $mois[sprintf("%02d", $current['mois']).'/'.$current['ans']].',';
                } else {
                    $info['all']['valeur'] .= '0,';
                }

                $info['all']['nbreValeur']++;

                if ($current['mois'] == 12) {
                    $current['mois'] = 1;
                    $current['ans']++;
                } else {
                    $current['mois']++;
                }
            } while ($current['mois'] != $fin['mois'] OR $current['ans'] != $fin['ans']);

            $info['all']['label'] = substr($info['all']['label'], 0, strlen($info['all']['label'])-1);
            $info['all']['valeur'] = substr($info['all']['valeur'], 0, strlen($info['all']['valeur'])-1);

            #######################################
        } else {
            $budget = Budget::where('compte', '=', $compte['id'])->where('id', '=', $budgetID)->with('categories')->first();

            $info['budget'] = $budget;

            $catListe = array();
            foreach ($budget['categories'] as $cat) {
                array_push($catListe, $cat->id);
            }

            $lignes = Ligne::where('compte', '=', $compte['id'])
                ->where('type', '=', 'debit')
                ->where('date', '>=', Session::get('periodeDebut'))
                ->where('date', '<=', Session::get('periodeFin'))
                ->whereIn('categorie', $catListe)
                ->get();

            $mois = array();

            foreach ($lignes as $ligne) {
                if (isset($mois[date('m/Y', $ligne->date)])) {
                    $mois[date('m/Y', $ligne->date)] += abs($ligne->montant);
                } else {
                    $mois[date('m/Y', $ligne->date)] = abs($ligne->montant);
                }
            }

            $info['all']['label'] = '';
            $info['all']['valeur'] = '';

            $debut['mois'] = date('m', Session::get('periodeDebut'));
            $debut['ans'] = date('Y', Session::get('periodeDebut'));

            $fin['mois'] = date('m', Session::get('periodeFin'));
            $fin['ans'] = date('Y', Session::get('periodeFin'));

            $current = $debut;

            $info['all']['nbreValeur'] = 0;
            while (($current['mois'] <= $fin['mois'] OR $current['ans'] < $fin['ans']) AND $current['ans'] <= $fin['ans']) {
                $info['all']['label'] .= '"'.sprintf("%02d", $current['mois']).'/'.$current['ans'].'",';
                if (isset($mois[sprintf("%02d", $current['mois']).'/'.$current['ans']])) {
                    $info['all']['valeur'] .= $mois[sprintf("%02d", $current['mois']).'/'.$current['ans']].',';
                } else {
                    $info['all']['valeur'] .= '0,';
                }

                $info['all']['nbreValeur']++;

                if ($current['mois'] == 12) {
                    $current['mois'] = 1;
                    $current['ans']++;
                } else {
                    $current['mois']++;
                }
            }

            $info['all']['label'] = substr($info['all']['label'], 0, strlen($info['all']['label'])-1);
            $info['all']['valeur'] = substr($info['all']['valeur'], 0, strlen($info['all']['valeur'])-1);
        }

        return View::make('rapport_index', array(
            'url' => Config::get('app.url'),
            'user' => $user,
            'compte' => $compte,
            'budgetListe' => $budget_liste,
            'page' => $budgetID,
            'categories' => $categories,
            'info' => $info,
            'periode' => $periode
        ));
    }
}
?>