<?php
class Core extends BaseController {

    public function login() {

        if (Input::has('login')) {
            if (Auth::attempt(array('login' => Input::get('login'), 'password' => Input::get('mdp'))))
            {
                return Redirect::to('/comptes');
            }
            else
            {
                return View::make('login', array('erreur' => "Erreur d'authentification, veuillez réessayer."));
            }
        }

        return View::make('login');
    }

    public function createUser() {
        if (Input::has('mdp')) {
            return View::make('create_user', array('mdp' => Hash::make(Input::get('mdp'))));
        }

        return View::make('create_user');
    }

    public function deco() {
        Auth::logout();
        return Redirect::to('/');
    }
}