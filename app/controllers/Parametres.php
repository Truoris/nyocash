<?php
class Parametres extends BaseController {

    public function index() {
        $user['pseudo'] = Auth::user()->login;
        $user['apikey'] = Auth::user()->apikey;

        $retour['compte'] = "";
        $retour['appli'] = "";

        if (Input::has('modifCompte')) {
            if (Input::get('mdp1') != NULL) {
                if (Input::get('mdp1') == Input::get('mdp2')) {
                    User::where('id', '=', Auth::user()->id)->update(array('login' => Input::get('pseudo'), 'password' => Hash::make(Input::get('mdp1'))));
                    $retour['compte'] = "ok";
                } else {
                    $retour['compte'] = "Les deux mots de passe ne sont pas identiques";
                }
            } else {
                User::where('id', '=', Auth::user()->id)->update(array('login' => Input::get('pseudo')));
                $retour['compte'] = "ok";
            }

            $user['pseudo'] = Input::get('pseudo');
        }

        if (Input::has('regen')) {
            $user['apikey'] = substr(md5(rand(1, 1000).time().Auth::user()->login), 0, 12);
            User::where('id', '=', Auth::user()->id)->update(array('apikey' => $user['apikey']));
        }

        if (Input::has('modifAppli')) {
            if (strlen(Input::get('pin1')) == 5 AND strpos(Input::get('pin1'), "0") === false) {
                if (Input::get('pin1') == Input::get('pin2')) {
                    User::where('id', '=', Auth::user()->id)->update(array('pin' => Input::get('pin1')));
                    $retour['appli'] = "ok";
                } else {
                    $retour['appli'] = "Les deux code PIN ne sont pas identiques";
                }
            } else {
                $retour['appli'] = "Code PIN incorrect : 5 chiffres, sans 0";
            }
        }

        return View::make('parametres', array('url' => Config::get('app.url'), 'user' => $user, 'retour' => $retour));
    }
}