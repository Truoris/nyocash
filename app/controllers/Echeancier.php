<?php

class Echeancier extends BaseController {

    public function run() {
        $echeances = Echeance::where('date_debut', '<', time())->with('last')->get();

        if (count($echeances) > 0) {

            foreach ($echeances as $each) {
                $each->periode = json_decode($each->periode);

                if ($each->periodicite == "jours") {

                    if ($each->last_entry > 0) {
                        $date = $each->Last->date+86400;
                    } else {
                        $date = $each->date_debut;
                    }

                    $jour = date('d', $date);
                    $mois = date('m', $date);
                    $an = date('Y', $date);

                    $i = 0;

                    while (($jour <= date('d') OR $mois != date('m') OR $an != date('Y')) AND $i < 1000) {
                        $i++;

                        $ligne = new Ligne();

                        $ligne->compte = $each->compte;
                        $ligne->tier = $each->tier;
                        $ligne->categorie = $each->categorie;
                        $ligne->type = $each->type;
                        $ligne->date = mktime(12, 0, 0, $mois, $jour, $an);
                        $ligne->compense = 0;
                        $ligne->montant = $each->montant;

                        $ligne->save();

                        $date += 86400;

                        $jour = date('d', $date);
                        $mois = date('m', $date);
                        $an = date('Y', $date);
                    }

                    if ($i > 0) {
                        Echeance::where('id', '=', $each->id)->update(array('last_entry' => $ligne->id));
                    }
                } elseif ($each->periodicite == "semaines") {

                    if ($each->last_entry > 0) {
                        $date = $each->Last->date+604800;
                    } else {
                        $jourSem = date('N', $each->date_debut);

                        if ($jourSem == $each->periode->jour) {
                            $date = $each->date_debut;
                        } else {
                            $deltaJour = 7-($jourSem-$each->periode->jour);
                            $date = $each->date_debut+$deltaJour*86400;
                        }
                    }

                    $jour = date('d', $date);
                    $mois = date('m', $date);
                    $an = date('Y', $date);

                    $i = 0;


                    while (($jour <= date('d') OR $mois != date('m') OR $an != date('Y')) AND $i < 160) {
                        $i++;

                        $ligne = new Ligne();

                        $ligne->compte = $each->compte;
                        $ligne->tier = $each->tier;
                        $ligne->categorie = $each->categorie;
                        $ligne->type = $each->type;
                        $ligne->date = mktime(12, 0, 0, $mois, $jour, $an);
                        $ligne->compense = 0;
                        $ligne->montant = $each->montant;

                        $ligne->save();

                        $date += 604800;

                        $jour = date('d', $date);
                        $mois = date('m', $date);
                        $an = date('Y', $date);
                    }


                    if ($i > 0) {
                        Echeance::where('id', '=', $each->id)->update(array('last_entry' => $ligne->id));
                    }
                } elseif ($each->periodicite == "mois") {

                    if ($each->last_entry > 0) {
                        $mois = date('m', $each->Last->date)+1;
                        $an = date('Y', $each->Last->date);
                    } else {
                        $mois = date('m', $each->date_debut);
                        $an = date('Y', $each->date_debut);

                        if (date('d', $each->date_debut) > $each->periode->jour) {
                            $mois++;
                        }
                    }

                    $jour = intval($each->periode->jour);
                    if ($jour == 31) {
                        $jour = date('t', mktime(12, 0, 0, $mois, 2, $an));
                    }

                    $i = 0;

                    while (($mois <= date('m') OR $an < date('Y')) AND $i < 40) {
                        if ($an < date('Y') OR $mois < date('m') OR $jour <= date('d')) {
                            $i++;

                            $ligne = new Ligne();

                            $ligne->compte = $each->compte;
                            $ligne->tier = $each->tier;
                            $ligne->categorie = $each->categorie;
                            $ligne->type = $each->type;

                            $ligne->date = mktime(12, 0, 0, $mois, $jour, $an);
                            $ligne->compense = 0;
                            $ligne->montant = $each->montant;

                            $ligne->save();
                        }

                        $mois++;

                        if ($mois == 13) {
                            $mois = 1;
                            $an++;
                        }

                        if (intval($each->periode->jour) == 31) {
                            $jour = date('t', mktime(12, 0, 0, $mois, 2, $an));
                        }
                    }

                    if ($i > 0) {
                        Echeance::where('id', '=', $each->id)->update(array('last_entry' => $ligne->id));
                    }
                } elseif ($each->periodicite == "ans") {
                    if ($each->last_entry > 0) {
                        $an = date('Y', $each->Last->date)+1;
                    } else {
                        $an = date('Y', $each->date_debut);

                        if (date('d', $each->date_debut) > $each->periode->jour AND date('m', $each->date_debut) >= $each->periode->mois) {
                            $an++;
                        }
                    }

                    $i = 0;

                    while ($an <= date('Y') AND $i < 5) {
                        if (date('n') >= $each->periode->mois OR $an < date('Y')) {
                            if (date('d') >= $each->periode->jour OR date('n') > $each->periode->mois OR $an < date('Y')) {
                                $i++;

                                $ligne = new Ligne();

                                $ligne->compte = $each->compte;
                                $ligne->tier = $each->tier;
                                $ligne->categorie = $each->categorie;
                                $ligne->type = $each->type;

                                $jour = intval($each->periode->jour);

                                if ($jour == 31) {
                                    $jour = date('t', mktime(12, 0, 0, $each->periode->mois, 2, $an));
                                }

                                $ligne->date = mktime(12, 0, 0, $each->periode->mois, $jour, $an);
                                $ligne->compense = 0;
                                $ligne->montant = $each->montant;

                                $ligne->save();
                            }
                        }

                        $an++;
                    }

                    if ($i > 0) {
                        Echeance::where('id', '=', $each->id)->update(array('last_entry' => $ligne->id));
                    }
                }


                $solde = round(Ligne::where('compte', '=', $each->compte)->sum('montant'), 2 );

                Compte::where('id', '=', $each->compte)->update(array('solde' => $solde));

            }
        }
    }
}