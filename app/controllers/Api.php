<?php
class Api extends BaseController {

    public function init() {
        if(Input::has('data')) {
            $data = json_decode(Input::get('data'));

            $user = User::where('login', '=', $data->login)->first();

            if (count($user) == 1 ) {
                if ($user['apikey'] == $data->key) {
                    $json['success'] = "true";
                } else {
                    $json['success'] = "false";
                    $json['msg'] = "La clé d'API est incorrect";
                }
            } else {
                $json['success'] = "false";
                $json['msg'] = "Utilisateur inconnu";
            }

            return Response::json($json, 200, array('Access-Controll-Allow-Origin' => '*'));
        }
    }

    public function login() {
        App::make('Echeancier')->run();

        if(Input::has('data')) {
            $data = json_decode(Input::get('data'));

            $user = User::where('login', '=', $data->login)->first();

            if (count($user) == 1 ) {
                if ($user['apikey'] == $data->key){
                    if ($user['pin'] == $data->pin) {

                        $json['success'] = "true";
                        $json['userID'] = $user['id'];

                        $nbreCat = Categorie::where('owner', '=', $user['id'])->where('timestamp', '>=', $data->lastSync)->count();
                        $nbreTier = Tier::where('owner', '=', $user['id'])->where('timestamp', '>=', $data->lastSync)->count();

                        $json['sync'] = 0;

                        if ($nbreCat > 0 OR $nbreTier > 0) {
                            $json['sync'] = 1;
                        }
                    } else {
                        $json['success'] = "false";
                        $json['msg'] = "Code d'accès incorrect";
                    }
                } else {
                    $json['success'] = "false";
                    $json['msg'] = "La clé d'API est incorrect";
                }
            } else {
                $json['success'] = "false";
                $json['msg'] = "Utilisateur inconnu";
            }

            return Response::json($json, 200, array('Access-Controll-Allow-Origin' => '*'));
        }
    }

    public function comptes() {
        if(Input::has('data')) {
            $data = json_decode(Input::get('data'));

            $user = User::where('login', '=', $data->login)->first();

            if (count($user) == 1 ) {
                if ($user['apikey'] == $data->key AND $user['pin'] == $data->pin){
                    $json['success'] = "true";
                    $comptes = Compte::where('owner', '=' , $user['id'])->get();

                    $json['comptes']['courant'] = array();
                    $json['comptes']['epargne'] = array();
                    $json['comptes']['pro'] = array();

                    foreach ($comptes as $compte) {
                        $infoCompte['nom'] = $compte->nom;
                        $infoCompte['solde'] = round($compte->solde, 2);
                        $infoCompte['compteID'] = $compte->id;

                        array_push($json['comptes'][$compte->type], $infoCompte);
                    }
                } else {
                    $json['success'] = "false";
                    $json['msg'] = "Authentification impossible";
                }
            } else {
                $json['success'] = "false";
                $json['msg'] = "Utilisateur inconnu";
            }

            return Response::json($json, 200, array('Access-Controll-Allow-Origin' => '*'));
        }
    }

    public function ligne() {
        if(Input::has('data')) {
            $data = json_decode(Input::get('data'));

            $user = User::where('login', '=', $data->login)->first();
            $compte = Compte::where('owner', '=', $user['id'])->where('id', '=', $data->compte)->first();

            if (count($user) == 1 ) {
                if ($user['apikey'] == $data->key AND $user['pin'] == $data->pin){
                    $json['success'] = "true";

                    $type = $data->data->type;
                    $montant = $data->data->montant;

                    if ($type == "debit") {
                        $montant *= -1;
                    }

                    $ligne = new Ligne();

                    $ligne->compte = $compte['id'];

                    $tier = Tier::where('nom', '=', $data->data->tier)->where('owner', '=', $user['id'])->first();

                    if (count($tier) == 0) {
                        $tier = new Tier();

                        $tier->owner = $user['id'];
                        $tier->nom = $data->data->tier;
                        $tier->timestamp = time();

                        $tier->save();
                    }

                    $ligne->tier = $tier->id;

                    $cat = Categorie::where('nom', '=', $data->data->cat)->where('owner', '=', $user['id'])->first();

                    if (count($cat) == 0) {
                        $cat = new Categorie();

                        $cat->owner = $user['id'];
                        $cat->nom = $data->data->cat;
                        $cat->timestamp = time();

                        $cat->save();
                    }

                    $ligne->categorie = $cat->id;

                    $ligne->type = $type;

                    $date = explode('/', $data->data->date);
                    $ligne->date = mktime(12, 0, 0, $date[1], $date[0], $date[2]);

                    $ligne->compense = 0;
                    $ligne->montant = $montant;

                    $ligne->save();

                    $compte['solde'] += $montant;
                    Compte::where('id', '=', $compte['id'])->where('owner', '=', $user['id'])->update(array('solde' => $compte->solde));
                } else {
                    $json['success'] = "false";
                    $json['msg'] = "Authentification impossible";
                }
            } else {
                $json['success'] = "false";
                $json['msg'] = "Utilisateur inconnu";
            }

            return Response::json($json, 200, array('Access-Controll-Allow-Origin' => '*'));
        }
    }

    public function sync() {
        if(Input::has('data')) {
            $data = json_decode(Input::get('data'));

            $user = User::where('login', '=', $data->login)->first();

            if (count($user) == 1 ) {
                if ($user['apikey'] == $data->key AND $user['pin'] == $data->pin){
                    $json['success'] = "true";
                    $json['time'] = time();

                    $json['dico']['cats'] = array();
                    $cats = Categorie::where('owner', '=', $user['id'])->where('timestamp', '>=', $data->lastSync)->get();
                    foreach ($cats as $cat) {
                        array_push($json['dico']['cats'], $cat->nom);
                    }

                    $json['dico']['tiers'] =array();
                    $tiers = Tier::where('owner', '=', $user['id'])->where('timestamp', '>=', $data->lastSync)->get();
                    foreach ($tiers as $tier) {
                        array_push($json['dico']['tiers'], $tier->nom);
                    }
                } else {
                    $json['success'] = "false";
                }
            } else {
                $json['success'] = "false";
            }

            return Response::json($json, 200, array('Access-Controll-Allow-Origin' => '*'));
        }
    }
}