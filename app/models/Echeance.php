<?php

class Echeance extends Eloquent {

    protected $table = 'echeances';
    public $timestamps = false;

    public function getDateDebAttribute($value)
    {
        return date('d/m/Y', $this->attributes['date_debut']);
    }

    public function tier()
    {
        return $this->belongsTo('Tier', 'tier');
    }

    public function categorie()
    {
        return $this->belongsTo('Categorie', 'categorie');
    }

    public function last()
    {
        return $this->belongsTo('Ligne', 'last_entry');
    }
}