<?php

class Budget extends Eloquent {

    protected $table = 'budgets';
    public $timestamps = false;

    public function categories()
    {
        return $this->belongsToMany('Categorie');
    }

}