<?php

class Ligne extends Eloquent {

    protected $table = 'lignes';
    public $timestamps = false;

    public function getDateFormatAttribute($value)
    {
        return date('d/m/Y', $this->attributes['date']);
    }

    public function tier()
    {
        return $this->belongsTo('Tier', 'tier');
    }

    public function categorie()
    {
        return $this->belongsTo('Categorie', 'categorie');
    }
}