<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::any('/', 'Core@login');
Route::any('/deco.html', array('before' => 'auth', 'uses' => 'Core@deco'));

Route::any('/parametres', array('before' => 'auth', 'uses' => 'Parametres@index'));

Route::any('/comptes', array('before' => 'auth', 'uses' => 'Comptes@liste'));
Route::any('/comptes/{id}/view-{tri}.html', array('before' => 'auth', 'uses' => 'Comptes@view'));
Route::any('/comptes/{id}/del-{ligne}-{tri}.html', array('before' => 'auth', 'uses' => 'Comptes@delete'))->where('ligne', '[0-9]+');
Route::any('/comptes/{id}/echeancier.html', array('before' => 'auth', 'uses' => 'Comptes@echeancier'));
Route::any('/comptes/{compte}/echeancier/del-{id}.html', array('before' => 'auth', 'uses' => 'Comptes@delEch'))->where('id', '[0-9]+');

Route::any('/comptes/{id}/rapport-{budget}.html', array('before' => 'auth', 'uses' => 'Rapport@index'));

Route::any('/comptes/{id}/import.html', array('before' => 'auth', 'uses' => 'Qif@import'));
Route::any('/comptes/{id}/export.html', array('before' => 'auth', 'uses' => 'Qif@export'));

Route::any('/categories', array('before' => 'auth', 'uses' => 'Comptes@categories'));
Route::any('/categories/del-{id}.html', array('before' => 'auth', 'uses' => 'Comptes@delCat'))->where('id', '[0-9]+');

Route::any('/ajax/compenser.html', 'Ajax@compenser');
Route::any('/ajax/getLigne.html', 'Ajax@getLigne');
Route::any('/ajax/getEcheance.html', 'Ajax@getEcheance');
Route::any('/ajax/budget/delCat.html', 'Ajax@budgetDelCat');
Route::any('/ajax/load-lines.html', 'Ajax@loadLines');
Route::get('/ajax/echeancier.html', 'Echeancier@run');

Route::post('/api/init', 'Api@init');
Route::post('/api/login', 'Api@login');
Route::post('/api/comptes', 'Api@comptes');
Route::post('/api/ligne', 'Api@ligne');
Route::post('/api/sync', 'Api@sync');

Route::any('/createUser', 'Core@createUser');