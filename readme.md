# NyoCash


NyoCash is a web application for manage your bank account, currently available in french but a english version will come soon.


## Installation

Fisrt, you need to download NyoCash. Here is [last version](http://last-version).

Next, you will need [composer](https://getcomposer.org/).

Now you go to the NyoCash root directory and you run the command :

    composer.phar update

Go to the public/ directory and create the next folders : file/import/ and file/export/

Lets go install the database, you have to import the nyocash.sql file in DB.

For finish, you have to configure NyoCash :

 - app/config/app.php : debug mode, root URL, local and key

 - app/config/database.php : database connexion


## Security

I hardly recommand you to use NyoCash with https!

## License

NyoCash is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
